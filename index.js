// insertOne method

    db.Rooms.insertOne({
        name: "single",
        accomodates: 2,
        price: "1000",
        description: "A single room with all the basic necessities",
        room_available: "10",
        isAvailable: false
    })


// insertMany method

    db.Rooms.insertMany([{
        name: "double",
        accomodates: 3,
        price: "2000",
        description: "A room fit for a small family going on a vacation",
        room_available: "5",
        isAvailable: false
        },

        {
        name: "queen",
        accomodates: 4,
        price: "4000",
        description: "A room with a queen sized bed perfect for a simple getaway",
        room_available: "15",
        isAvailable: false
        }
    ])

// Use the find method to search for a room with the name double.

    db.Rooms.find({name: "double"});

// Use the updateOne method to update the queen room and set the available rooms to 0.

    db.Rooms.updateOne(
        { name: "queen"},
        {
            $set:{
                name:"queen",
                accomodates: 4,
                price: "4000",
                description: "A room with a queen sized bed perfect for a simple getaway",
                room_available: "0",
                isAvailable: false
            }
        }
    );

// Use the deleteMany method rooms to delete all rooms that have 0 availability.

    db.Rooms.deleteMany({room_available: "0"});